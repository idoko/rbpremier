Rails.application.routes.draw do
  devise_for :admins, path: 'admin',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
             }

  devise_for :users,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
             },
             controllers: {
                 sessions: 'sessions',
                 registration: 'registrations'
             }
  resources :teams, param: :slug
  resources :fixtures
  get "search" => "search#search"
end
