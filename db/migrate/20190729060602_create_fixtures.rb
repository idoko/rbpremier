class CreateFixtures < ActiveRecord::Migration[5.2]
  def change
    create_table :fixtures do |t|
      t.integer :home
      t.integer :away
      t.string :venue
      t.timestamp :kickoff_time

      t.timestamps
    end
  end
end
