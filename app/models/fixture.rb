class Fixture < ApplicationRecord
  validates_presence_of :home, :away, :venue, :kickoff_time
end
