class Team < ApplicationRecord
  validates_presence_of :name, :bio, :logo
end
