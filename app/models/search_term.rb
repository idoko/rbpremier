class SearchTerm
  attr_reader :where_clause, :where_args, :order

  def initialize(search_term)
    @where_clause = ""
    @where_args = {}
    build_for_team_name_search(search_term)
  end

  private
  def build_for_team_name_search(search_term)
    @where_clause << search_text(:name)
    @where_args[:name] = starts_with(search_term)
    @where_clause << " OR #{search_text(:bio)}"
    @where_args[:bio] = starts_with(search_term)
  end

  def search_text(field)
    "#{field} like :#{field}"
  end

  def starts_with(term)
    term + "%"
  end
end