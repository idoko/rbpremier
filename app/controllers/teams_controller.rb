class TeamsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_team, only: [:show, :update, :destroy]

  def index
    @teams = Team.all
    render json: {data: @teams}
  end

  def create
    @team = Team.new(name: params[:name],
                     bio: params[:bio],
                     logo: params[:logo],
                     slug: params[:slug])
    @team.save
    render json: {data: @team}
  end

  def destroy
    @team.delete
    head :ok
  end

  def update
    @team.update(
             name: params[:name],
             bio: params[:bio],
             logo: params[:logo],
             slug: params[:slug]
    )
    head :ok
  end

  def show
    render json: {data: @team}
  end

  private
    def set_team
      @team = Team.find_by! slug: params[:slug]
    end
end