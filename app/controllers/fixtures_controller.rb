class FixturesController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_fixture, only: [:show, :update, :destroy]

  def create
    home_team = Team.find_by slug: params[:home]
    away_team = Team.find_by slug: params[:away]

    @fixture = Fixture.new(
                          home: home_team.id,
                          away: away_team.id,
                          venue: params[:venue],
                          kickoff_time: DateTime.parse(params[:kickoff])
    )
    render json: {data: @fixture}
  end

  def show
    render json: {data: @fixture}
  end

  def update
    home_team = Team.find_by slug: params[:home]
    away_team = Team.find_by slug: params[:away]
    @fixture.update(
        home: home_team.id,
        away: away_team.id,
        venue: params[:venue],
        kickoff_time: DateTime.parse(params[:kickoff])
    )
    head :ok
  end

  def destroy
    @fixture.delete
    head :ok
  end

  private
  def set_fixture
    @fixture = Fixture.find params[:id]
  end

end