class SearchController < ApplicationController
  PAGE_SIZE = 15

  def search
    page = (params[:page] || 0).to_i

    if params[:keywords].present?
      @keywords = params[:keywords]
      search_term = SearchTerm.new(@keywords)
      @matches = Team.where(
                      search_term.where_clause,
                      search_term.where_args
      )
        .order(search_term.order)
        .offset(PAGE_SIZE * page).limit(PAGE_SIZE)
    else
      @matches = []
    end
    render json: {data: {results: @matches}}
  end
end
