require 'rails_helper'

RSpec.describe Team, type: :model do
  subject {
    described_class.new(
                       name: "team A",
                       bio: "mater dei",
                       logo: "team-a.png"
    )
  }

  it "is valid with valid attrs" do
    expect(subject).to be_valid
  end

  it "is invalid without a name" do
    subject.name = nil
    expect(subject).to_not be_valid
  end

  it "is invalid without a bio" do
    subject.bio = nil
    expect(subject).to_not be_valid
  end

  it "is invalid without a logo path" do
    subject.logo = nil
    expect(subject).to_not be be_valid
  end
end