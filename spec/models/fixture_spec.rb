require 'rails_helper'

RSpec.describe Fixture, type: :model do
  subject {
    described_class.new(
        home: "team-a", away: "team-b", venue: "02 Arena",
        kickoff_time: "2019-01-01 05:00"
    )
  }

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is invalid without a home team" do
    subject.home = nil
    expect(subject).to_not be_valid
  end

  it "is invalid without an away team" do
    subject.away = nil
    expect(subject).to_not be_valid
  end

  it "is invalid without a venue" do
    subject.venue = nil
    expect(subject).to_not be_valid
  end

  it "is invalid without a kick-off time" do
    subject.kickoff_time = nil
    expect(subject).to_not be_valid
  end
end
